function validation() {
    var name = document.getElementById('Name').value;
    var email = document.getElementById('Email').value;
    var password = document.getElementById('Password').value;
    var cpassword = document.getElementById('CPassword').value;


    var checkname = /^[A-Za-z .]{3,30}$/;
    var ckeckemail = /[A-za-z0-9.@#$!*_]{3,}@[A-Za-z]{3,}[.]{1}[A-za-z.]{2,}/;
    var checkpassword = /^(?=.*[0-9])(?=.*[!@#$%^&*])[A-Za-z0-9!@#$%^&* .]{8,20}$/;

    if (checkname.test(name)) {
        document.getElementById('name_error').innerHTML = ""
    }
    else {
        document.getElementById('name_error').innerHTML = "Alphabets Only";
        return false;
    }

    if (ckeckemail.test(email)) {
        document.getElementById('email_error').innerHTML = ""
    }
    else {
        document.getElementById('email_error').innerHTML = "Invalid Email";
        return false;
    }
    if (checkpassword.test(password)) {
        document.getElementById('password_error').innerHTML = ""
    }
    else {
        document.getElementById('password_error').innerHTML = "Must have one special char and min length of 8 char";
        return false;
    }
    if (cpassword.match(password)) {
        document.getElementById('cpassword_error').innerHTML = ""
    }
    else {
        document.getElementById('cpassword_error').innerHTML = "Password must match";
        return false;
    }


}